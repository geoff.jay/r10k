#lookup('classes', {merge => unique}).include

# Define the 'main' filebucket
filebucket { 'main':
  server => $::settings::server,
  path   =>  false
}

# Make filebucket 'main' the default backup location for File resources
File { backup => 'main' }

# The default node matches any node that does not have a more specific node
# definition.
node default {
}
