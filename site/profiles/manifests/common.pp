class profile::common {
  users { 'common': }

  include profiles::ssh::server
  # TODO: Test firewall configuration
  #include profiles::firewall::setup

  # common packages needed everywhere
  package {[
      'vim',
      'sudo',
      'screen'
    ]:
    ensure => present,
  }

  # set locale
  class { 'locales':
    default_locale => 'en_US.UTF-8',
    locales        => ['en_US.UTF-8 UTF-8'],
  }

  # TODO: Add a role/profile for NTP
  #class { '::ntp': }
}
