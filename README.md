# Puppet R10k Repository

This was created to be used with a Puppet master as installed by Foreman. After
making changes to the repository push to the appropriate branch and follow the
steps in the [Update](#update) section.

## Setup

These are the rough steps used to setup the server for use with this repo.

<!--
Consider moving this into it's own repo with a Gemfile to setup,
see here - https://techpunch.co.uk/development/how-to-build-a-puppet-repo-using-r10k-with-roles-and-profiles
-->
```sh
useradd -d /home/r10k -m -s /bin/bash r10k
gem install r10k --no-rdoc --no-ri
mkdir /etc/puppetlabs/r10k
echo << EOF > /etc/puppetlabs/r10k/r10k.yaml
:cachedir: '/var/cache/r10k'
:sources:
  :base:
    remote: 'https://gitlab.com/geoff.jay/r10k.git'
    basedir: '/etc/puppetlabs/code/environments'
EOF
echo << EOF > /etc/puppetlabs/puppet/hiera.yaml
---
version: 5
defaults:
  datadir: data
  data_hash: yaml_data
EOF
mkdir -p /var/cache/r10k
chgrp r10k /var/cache/r10k
chmod 775 /var/cache/r10k
chgrp r10k /etc/puppetlabs/code/environments/
chmod 775 /etc/puppetlabs/code/environments/
```

## Update

After logging into the Foreman host the environments can be updated with

```sh
su - r10k
r10k deploy environment --puppetfile
```
