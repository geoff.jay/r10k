#!/bin/bash
if [ -e $1/$2/.r10k-deploy.json ]
then
  /opt/puppetlabs/puppet/bin/ruby $1/$2/scripts/config_version.rb $1 $2
fi
